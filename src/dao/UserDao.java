package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	//新規登録画面で入力した内容をデータベースに登録
	public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    ?, ");
            sql.append("    CURRENT_TIMESTAMP, ");  // created_date
            sql.append("    CURRENT_TIMESTAMP ");       // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
     }

	 //ログイン画面で入力された情報をデータベースから持ってくる
	 public User select(Connection connection, String account, String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE account = ? AND password = ?";

	        ps = connection.prepareStatement(sql);

	        ps.setString(1, account);
	        ps.setString(2, password);

	        ResultSet rs = ps.executeQuery();

	        List<User> users = toUsers(rs);
	        if (users.isEmpty()) {
	            return null;
	        } else if (2 <= users.size()) {
	            throw new IllegalStateException("ユーザーが重複しています");
	        } else {
	            return users.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	 }

	 //ログイン画面で入力された情報をデータベースから持ってきた後Userのリストに詰める
	 private List<User> toUsers(ResultSet rs) throws SQLException {

	    List<User> users = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            User user = new User();
	            user.setId(rs.getInt("id"));
                user.setAccount(rs.getString("account"));
                user.setPassword(rs.getString("password"));
                user.setName(rs.getString("name"));
                int branch = Integer.parseInt(rs.getString("branch_id"));
                int department = Integer.parseInt(rs.getString("department_id"));
                user.setIsStopped(rs.getInt("is_stopped"));
                user.setBranchId(branch);
                user.setDepartmentId(department);

                users.add(user);
	        }
	        return users;
	    } finally {
	        close(rs);
	    }
	}

	 //ユーザー管理画面にログインユーザーの情報を保持＆表示のための処理
	 public User select(Connection connection, int id) {

		 	//Servletから指定してきたidのユーザーの情報をSQLから取り出す
		    PreparedStatement ps = null;
		    try {
		        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);

	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();

	        List<User> users = toUsers(rs);
	        if (users.isEmpty()) {
	            return null;
	        } else if (2 <= users.size()) {
	            throw new IllegalStateException("ユーザーが重複しています");
	        } else {
	            return users.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	 //ユーザー編集画面で更新ボタン押された後動く処理(update用)
	 public void update(Connection connection, User user) {

		    PreparedStatement ps = null;
		    try {
		        StringBuilder sql = new StringBuilder();
		        sql.append("UPDATE users SET ");
		        sql.append("    account = ?, ");
		        sql.append("    password = ?, ");
		        sql.append("    name = ?, ");
		        sql.append("    branch_id = ?, ");
		        sql.append("    department_id = ?, ");
		        sql.append("    updated_date = CURRENT_TIMESTAMP ");
		        sql.append("WHERE id = ?");

		        ps = connection.prepareStatement(sql.toString());

		        ps.setString(1, user.getAccount());
		        ps.setString(2, user.getPassword());
		        ps.setString(3, user.getName());
		        ps.setInt(4, user.getBranchId());
		        ps.setInt(5, user.getDepartmentId());
		        ps.setInt(6, user.getId());

		        int count = ps.executeUpdate();
		        if (count == 0) {
		            throw new NoRowsUpdatedRuntimeException();
		        }
		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(ps);
		    }
	 }

	 //ユーザー編集画面で更新ボタン押された後動く処理(update用)
	 public void insert(Connection connection, int stopped, int userId) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("UPDATE users SET ");
	        sql.append("    is_stopped = ? ");
	        sql.append("WHERE id = ?");

	        ps = connection.prepareStatement(sql.toString());

	        //user.getIdとかにするのは引数で直接値を受け渡しておらず、
	        //userという一つの箱の中に情報を詰めているから取り出す作業をしている処理。
	        ps.setInt(1, stopped);
	        ps.setInt(2, userId);

	        int count = ps.executeUpdate();
	        if (count == 0) {
	            throw new NoRowsUpdatedRuntimeException();
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
}
