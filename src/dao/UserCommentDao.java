package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

	//データベースのcommentsテーブルから情報取り出し、
	//commentsテーブルのuser.idとusersテーブルのidをキーに表を結合
    public List<UserComment> select(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
        	StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("    comments.id as id, ");
            sql.append("    comments.text as text, ");
            sql.append("    comments.message_id as message_id, ");
            sql.append("    users.id as user_id, ");
            sql.append("    users.account as account, ");
            sql.append("    users.name as name, ");
            sql.append("    comments.created_date as created_date, ");
            sql.append("    comments.updated_date as updated_date ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
	        sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();

            //UserComment.javaをcommentsという名前にし、そこに情報詰める
            List<UserComment> comments = toUserComment(rs);
            return comments;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //messages、usersテーブルを結合したテーブルから持ってきた情報を、
    //UserMessage.javaに詰める(結合したからusersテーブルの情報も持ってこれる)
    private List<UserComment> toUserComment(ResultSet rs) throws SQLException {

        List<UserComment> comments = new ArrayList<UserComment>();
        try {
        	while (rs.next()) {
                UserComment comment = new UserComment();
                comment.setId(rs.getInt("id"));
                comment.setText(rs.getString("text"));
                comment.setMessageId(rs.getInt("message_id"));
                comment.setUserId(rs.getInt("user_id"));
                comment.setAccount(rs.getString("account"));
                comment.setName(rs.getString("name"));
                comment.setCreatedDate(rs.getTimestamp("created_date"));
                comment.setUpdatedDate(rs.getTimestamp("updated_date"));

                comments.add(comment);
            }
            return comments;
        } finally {
            close(rs);
        }
    }
}

