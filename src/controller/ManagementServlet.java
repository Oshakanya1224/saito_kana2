package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserBranchDepartment;
import service.UserService;

//URL指定
@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	//management.jspの表示
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

	//データベースから取得した投稿情報をServiceから受け取る
	List<UserBranchDepartment> managements = new UserService().select();

	//ダブルコーテーション内はjspと対応
	//ここでjspと繋ぎ合わせ
    request.setAttribute("managements", managements);

    //jsp指定
    request.getRequestDispatcher("management.jsp").forward(request, response);
    }
}
