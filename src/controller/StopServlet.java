package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

	//ユーザー復活停止
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	//jspから受け取るとString型なので数値は変換する必要がある

    	//jspから青文字のやつをキーにstoppedの中の1受け取り
        int stopped = Integer.parseInt(request.getParameter("stopped"));

        //jspから青文字のやつをキーにstartの中の0受け取り
        int userId = Integer.parseInt(request.getParameter("userId"));

        //Daoでuserのidをキーに値を入れていくのでuserのidも送る
        new UserService().insert(stopped, userId);
        response.sendRedirect("./management");
    }
}
