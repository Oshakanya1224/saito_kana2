package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;


@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

	//login.jsp表示
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

	//ログインボタン押された後の処理
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String account = request.getParameter("account");
        String password = request.getParameter("password");
        List<String> errorMessages = new ArrayList<String>();

        //ログイン画面で入力された情報を変数userに詰めてserviceへ
    	User user = new UserService().select(account, password);

    	//一番下にあるbooleanメソッドに振り分け条件をまとめていて、
    	//振り分け後の処理をここにまとめている(前回の課題と照らし合わせてみると理解しやすい)
        if (!isValid(request, errorMessages)) {
            request.setAttribute("errorMessages", errorMessages);
            //accounの保持
            request.setAttribute("account", account);
            request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        //停止中のアカウントのとき
        if (user.getIsStopped() == 1) {
        	errorMessages.add("ログインできません");
        	request.setAttribute("errorMessages", errorMessages);
        	request.getRequestDispatcher("login.jsp").forward(request, response);
            return;
        }

        HttpSession session = request.getSession();
        session.setAttribute("loginUser", user);
        response.sendRedirect("./");
    }

    private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

    	String account = request.getParameter("account");
        String password = request.getParameter("password");
        //違う島で使うので定義し直し
        User user = new UserService().select(account, password);

        //アカウント
        if (StringUtils.isBlank(account)) {
        	//アカウントが空白だったら
            errorMessages.add("アカウント名が入力されていません");
        } else if ((account.length() > 10))  {
        	//20文字以上だったら
            errorMessages.add("アカウント名は10文字以下で入力してください");
        }

        //パスワード
        if (StringUtils.isBlank(password)) {
            errorMessages.add("パスワードが入力されていません");
        }

        //存在しないアカウントorパスワード入力されたとき
        if (user == null) {
            errorMessages.add("アカウント名またはパスワードが間違っています");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }
}
