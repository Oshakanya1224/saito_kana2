package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branches;
import dao.BranchDao;

public class BranchService {
	//Daoでデータベースから取得した内容を受け取ってServletに渡す
    public List<Branches> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Branches> branches = new BranchDao().select(connection);
            commit(connection);

            return branches;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
