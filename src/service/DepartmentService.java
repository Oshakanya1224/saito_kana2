package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Departments;
import dao.DepartmentDao;

public class DepartmentService {
	//Daoでデータベースから取得した内容を受け取ってServletに渡す
    public List<Departments> select() {

        Connection connection = null;
        try {
            connection = getConnection();
            List<Departments> departments = new DepartmentDao().select(connection);
            commit(connection);

            return departments;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
