package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter({"/setting", "/management", "/signup"})
public class ManagementFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		//これから使う変数たちを定義
        HttpSession session = ((HttpServletRequest)request).getSession();
        List<String> errorMessages = new ArrayList<String>();

        //"loginUser"という名前のsessionに詰めていた情報をloginUserという新しい変数に入れ込む
		User loginUser = (User) session.getAttribute("loginUser");

		//loginUserから取り出す情報を変数に入れて使う
		int branchId = loginUser.getBranchId();
		int departmentId = loginUser.getDepartmentId();

		//loginUserのbranchIdとdepartmentIdが共に1じゃないとき
		if (!((branchId == 1) || (departmentId == 1))) {
	        //セッションがNullならば、ログイン画面へ飛ばす
			errorMessages.add("管理者権限がありません");
			session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse)response).sendRedirect("./");
	        return;
	    }
		// サーブレットを実行
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig config) throws ServletException{
	}

	@Override
	public void destroy() {
	}
}
