<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ホーム画面</title>
</head>
<body>
	<c:if test="${ not empty errorMessages }">
        <div class="errorMessages">
            <ul>
                <c:forEach items="${errorMessages}" var="errorMessage">
                    <li><c:out value="${errorMessage}" />
                </c:forEach>
                <c:remove var="errorMessages" scope="session" />
            </ul>
        </div>
    </c:if>

	<div class="header">
		<c:if test="${ empty loginUser }">
			<!-- hrefにはURL書く(jspではない) -->
	       	<a href="login">ログイン</a>
	       	<a href="signup">登録する</a>
	   	</c:if>
	    <c:if test="${ not empty loginUser }">
	        <a href="message">新規投稿</a>
	        <a href="management">ユーザー管理</a>
	        <a href="logout">ログアウト</a><br /><br />
		</c:if>
	</div>

	<div class="serch">
		<form action="index.jsp" method="get">
			<!-- 日付検索 -->
			<label for="date">日付</label>
			<input type="date" name="start" id="start" value="${start}" />から
			<input type="date" name="end" id="end" value="${end}" /><br /><br />

			<!-- カテゴリ検索 -->
			<label for="category">カテゴリ</label>
			<input name="category" id="category" value="${category}" />

			<input type="submit" value="絞り込み"><br/><br/>
		</form>
	</div>

	<c:if test="${ not empty loginUser }">
	    <div class="profile">
	        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
	        <div class="account"><h3>@<c:out value="${loginUser.account}" /></h3></div>
	    </div>
	</c:if>

	<div class="messages">
		<!-- HomeServletのmessagesの内容をここではmessageとして扱うよ -->
	    <c:forEach items="${messages}" var="message">
	        <div class="message">
                ユーザ名：<div class="name"><c:out value="${message.name}" /></div><br />
                件    名：<div class="title"><c:out value="${message.title}" /></div><br />
                カテゴリ：<div class="category"><c:out value="${message.category}" /></div><br />
	            投稿内容：<div class="text"><c:out value="${message.text}" /></div><br />
		        <div class="date">
	           		投稿日時:<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
	           	</div>

	           	<c:if test="${ not empty loginUser }">
		           	<form action="deleteMessage" method="post">
			           	<!-- messageIdをDaoまで送ってどのIDのメッセージを消すのか明示する -->
			           	<input type="hidden" name="messageId" value="${message.id}"><br/>
			            <input type="submit" value="投稿削除"><br /><br />
			        </form>
			    </c:if>
	        </div>

			<div class="comments">
				<!-- HomeServletのmessagesの内容をここではmessageとして扱うよ -->
	    		<c:forEach items="${comments}" var="comment">
	    			<c:if test="${comment.messageId == message.id}">
						ユーザー名：<div class="name"><c:out value="${comment.name}" /></div><br />
		            	コメント内容：<div class="text"><c:out value="${comment.text}" /></div><br />
		            	<div class="date">
           					コメント日時:<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br />
						</div>

		        		<c:if test="${ not empty loginUser }">
				           	<form action="deleteComment" method="post">
					           	<!-- commentIdをDaoまで送ってどのIDのコメントを消すのか明示する -->
					           	<input type="hidden" name="commentId" value="${comment.id}"><br/>
					            <input type="submit" value="コメント削除"><br /><br />
					        </form>
				    	</c:if>
				    </c:if>
	        	</c:forEach>
	        </div>

           	<!-- コメント投稿ボタン押されたときに動かすのはURLがcommentのServletのPostメソッド -->
           	<form action="comment" method="post">
           		<c:if test="${ not empty loginUser }">
		          	<div class="comment">
		           		<label for="text">コメント入力欄</label><br />
		           		<input type="hidden" name="messageId" value="${message.id}"><br/>
		            	<textarea name="text" cols="30" rows="5" class="tweet-box"></textarea><br />
		            	<input type="submit" value="コメント投稿"><br /><br />
		        	</div>
		        </c:if>
	        </form>
	    </c:forEach>
	</div>

	<div class="copyright"> Copyright(c)SaitoKana</div>
</body>
</html>