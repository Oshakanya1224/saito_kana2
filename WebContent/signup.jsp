<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ユーザー新規登録</title>
</head>

<body>
	 <c:if test="${ not empty errorMessages }">
	       <div class="errorMessages">
	           <ul>
	               <c:forEach items="${errorMessages}" var="errorMessage">
	                   <li><c:out value="${errorMessage}" />
	               </c:forEach>
	           </ul>
	           <c:remove var="errorMessages" scope="session" />
	       </div>
    </c:if>

	<div class="main-contents">
		<form action="signup" method="post"><br />
			<!-- ここはURLに対応 -->
			<a href="management">ユーザー管理</a> <br /><br />

            <label for="account">アカウント</label>
            <input name="account" id="account" value="${user.account}"/> <br />

   			<!-- type="password"で●化 -->
            <label for="password">パスワード</label>
            <input name="password" type="password" id="password" /> <br />

            <label for="confirmedPassword">確認用パスワード</label>
            <input name="confirmedPassword" type="password" id="confirmedPassword" /> <br />

            <label for="name">名前</label>
            <input name="name" id="name" value="${user.name}"/> <br />

			<!-- タグの間に入ってるのは表示するだけの支社名と部署名
			     実際はbranchIdとdepartmentIdをServletに送っている -->

			<p>支社<br>
			   <!-- selectタグでプルダウンメニュー作る -->
               <select size="1" name="branchId" id="branchId">
	               <c:forEach items="${branches}" var="branch">
	               		<!-- optionタグはプルダウンの中身に表示される項目 -->
	               		<!-- valueは選択項目を送信する際に受けとり手に値を知らせる -->
						<c:if test="${branch.id == user.branchId}">
               				<option value="${branch.id}" selected> ${branch.name}</option>
               			</c:if>
               			<c:if test="${branch.id != user.branchId}">
               				<option value="${branch.id}"> ${branch.name}</option>
               			</c:if>
		   		   </c:forEach>
		   	   </select>
			</p>

			<p>部署<br>
			   <select size="1" name="departmentId" id="departmentId">
			   	   <!-- このドルカッコ内はServletと対応 -->
			   	   <c:forEach items="${departments}" var="department">
			   	   		<!-- このドルカッコ内はbeansと対応 -->
               			<c:if test="${department.id == user.departmentId}">
               				<!-- selectedで保持したままにできる -->
               				<option value = "${department.id}" selected>${department.name}</option>
               			</c:if>
               			<c:if test="${department.id != user.departmentId}">
               				<option value = "${department.id}">${department.name}</option>
               			</c:if>
	               </c:forEach>
	           </select>
			</p>

               <input type="submit" value="登録" /> <br />
		</form>
		<div class="copyright"> Copyright(c)SaitoKana</div>
     </div>
</body>
</html>